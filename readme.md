# Mythic Table CLI

blah blah

## Setup

```
python -m pip install -e .
```

### Gcloud

Install the Google Cloud CLI/SDK

```
gcloud auth login
gcloud init
gcloud auth application-default login
```

## Usage

```
mt admin configure
MongoDB adress:  # This is just the domain name
MongoDB user:
Keycloak address: # example https://key.mythictable.com/auth/
Keycloak user:
Envc file created at ./.env

mt admin user delete user@example.com  # this will dry run it

mt admin user delete user@example.com  --dry-run=False
```


## Environment Variables

* `MT_MONGO_PASS`
* `MT_KEY_PASS`

## TODO

- [ ] Fix issue where the `.env` files doesn't exist when running configure
- [ ] Remove duplication around password prompts and env vars
- [ ] Move bucket name to env
- [x] How to identify which password is being prompted
- [ ] Improve dry run output by adding object type
- [ ] Improve error messaging
- [ ] Tests :P
- [ ] Replace Dry Run with a terraform like prompt
- [ ] Add defaults to the env vars
