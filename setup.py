from setuptools import setup, find_packages

setup(
    name="mythictable-cli",
    install_requires = [
        "fire",
        "pymongo",
        "python-dotenv",
        "python-keycloak",
        'google-cloud-storage'
    ],
    packages=find_packages(),
    entry_points = {
        "console_scripts": [
            "mt = mythictable.main:main"
        ]
    }
)