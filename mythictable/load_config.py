import os

from dotenv import load_dotenv

from mythictable import logger

ENV_FILE=".env"

def load_config():
    if not os.path.isfile(ENV_FILE):
        logger.warn(f"{ENV_FILE} does not exist!")
        return

    with open(ENV_FILE, 'r') as in_file:
        load_dotenv(stream=in_file)
