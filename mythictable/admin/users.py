from . import Login
from mythictable.connectors import MongoUser
from mythictable import logger

def _find_key_user_id(email: str):
    id = Login.keycloak().find_user(email)
    if id is not None:
        logger.info(f"User ID: {id['id']}")
        print(f"Found user with id {email}")
        return id['id']
    else:
        print("No such user found")
        raise SystemExit(1)

def _find_mongo_user(id: str) -> MongoUser:
    return Login.mongo().get_user(id)

class UserCommand:
    def search(self, email: str):
        id = _find_key_user_id(email)
        _find_mongo_user(id)
        

    def list(self, collection: str, email: str):
        user = _find_mongo_user(_find_key_user_id(email))
        for i in user.get_collections(collection):
            print(i)
    
    def files(self, email: str):
        user = _find_mongo_user(_find_key_user_id(email))
        gcp = Login.gcp()
        gcp.get_user_blobs(user.id)
    
    def delete(self, email: str, dry_run: bool = True):
        collect_list = ["campaign", "collections", "files"]
        key_id = _find_key_user_id(email)
        user = _find_mongo_user(key_id)
        gcp = Login.gcp()
        key = Login.keycloak()

        key.deactivate_user(key_id, dry_run)
        gcp.delete_user_blobs(user.id, dry_run)

        for i in collect_list:
            user.delete_collection(i, dry_run)
        user.delete_profile(dry_run)
        key.delete_user(key_id, dry_run)
