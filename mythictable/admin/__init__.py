from .login import Login
from .config import configure
from .users import UserCommand

admin_commands = {
    "login": Login,
    "configure": configure,
    "user": UserCommand,
}