import os

def configure():
    env_vars = [
        #Prompt, env var name
        ("MongoDB address", "MT_MONGO_ADDR"),
        ("MongoDB user", "MT_MONGO_USER"),
        ("MonogoDB Database", "MT_MONGO_DB"),
        ("Keycloak address", "MT_KEY_ADDR"),
        ("Keycloak user", "MT_KEY_USER"),
        ("GCP Bucket name", "MT_GCP_BUCKET"),
    ]

    with open('.env', 'w') as out_file:
        for pair in env_vars:
            if os.environ.get(pair[1]) is None:
                os.environ[pair[1]] = input(f"{pair[0]}: ")
            out_file.write(f'{pair[1]}={os.environ[pair[1]]}\n')
            
        