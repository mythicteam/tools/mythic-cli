import os

from pymongo import errors
import getpass

from .. import logger
from ..connectors import MongoConnection, KeycloakConnector, GCPConnector

class Login:
    def mongo():
        if os.environ.get('MT_MONGO_PASS') is None:
            password = getpass.getpass("MongoDB password: ")
        else:
            password = os.environ.get('MT_MONGO_PASS')
        try:
            return MongoConnection(
                os.environ.get('MT_MONGO_ADDR'),
                os.environ.get('MT_MONGO_USER'),
                password
            )
        except errors.OperationFailure as e:
            logger.fatal("Could not log in to Mongo")
            logger.fatal(e)
            raise RuntimeError

    
    def keycloak() -> KeycloakConnector:
        if os.environ.get('MT_KEY_PASS') is None:
            password = getpass.getpass("Keycloak password: ")
        else:
            password = os.environ.get('MT_KEY_PASS')

        return KeycloakConnector(
            os.environ.get('MT_KEY_ADDR'),
            os.environ.get('MT_KEY_USER'),
            password
        )
    
    def gcp():
        return GCPConnector()