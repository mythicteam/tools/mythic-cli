from .admin import admin_commands
from .load_config import load_config

import fire
import logging
import os

def main():
    logging.basicConfig(level=os.environ.get("LOGLEVEL", "WARNING").upper())
    logging.Logger(__package__)
    load_config()

    fire.Fire({
        "admin": admin_commands
    })
