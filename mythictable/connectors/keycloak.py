from keycloak import KeycloakAdmin, KeycloakOpenIDConnection

from mythictable import logger
from typing import Union

class KeycloakConnector:
    def __init__(self, host: str, uname: str, passwd: str, token: str = None):
        if token is not None:
            raise NotImplemented
        
        self.conn = KeycloakOpenIDConnection(
            server_url=host,
            username=uname,
            password=passwd,
            realm_name="MythicTable",
            user_realm_name="master",
            client_id="admin-cli",
            verify=True
        )

        self.admin = KeycloakAdmin(connection=self.conn, realm_name="MythicTable")

    def find_user(self, email: str) -> Union[str | None]:
        return self.admin.get_users({'email': email})[0]
    
    def deactivate_user(self, id: str, dry_run: bool):
        if dry_run:
            logger.warn(f"Not deactivating user {id} due to dry run")
        else:
            self.admin.disable_user(id)
            self.admin.user_logout(id)


    def delete_user(self, id: str, dry_run: bool):
        if dry_run:
            logger.warn(f"Skipping delete of {id} due to dry run")
        else:
            self.admin.delete_user(id)

