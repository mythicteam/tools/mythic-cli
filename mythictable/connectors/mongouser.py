from pymongo.database import Database
from bson.objectid import ObjectId

from mythictable import logger

class MongoUser:
    def __init__(self, payload: str, db: Database) -> None:
        self.db = db
        self.id = str(payload['_id'])
        self.key_id = payload['UserId']
        self.field_mapping = {
            'campaign': 'Owner',
            'collections': '_userid',
            'files': 'user',
        }
    
    def get_collections(self, collection: str):
        return self.db[collection].find({self.field_mapping[collection]: self.id})
    
    def delete_collection(self, collection: str, dry_run: bool):
        if dry_run:
            for item in self.get_collections(collection):
                logger.warn(f"Skipping delete of {item} due to dry run")
        else:
            self.db[collection].delete_many({self.field_mapping[collection]: self.id})
    
    def delete_profile(self, dry_run: bool):
        if dry_run:
            for profile in self.db['profiles'].find({'UserId': self.key_id}):
                logger.warn(f"Skipping delete of {profile} due to dry run")
        else:
            self.db['profiles'].delete_many({'UserId': self.key_id})