import os

from pymongo.mongo_client import MongoClient
from pymongo.server_api import ServerApi

from .mongouser import MongoUser
from ..logger import logger

class MongoConnection:
    def __init__(self, host: str, username: str, passwd: str) -> None:
        self.client = MongoClient(
            f"mongodb+srv://{username}:{passwd}@{host}/?retryWrites=true&w=majority",
            server_api=ServerApi('1')
        )

        #Ensure that the connection is functioning
        self.client.admin.command('ping')
        logger.info(self.client.list_database_names())
        self.db = self.client[f'{os.environ["MT_MONGO_DB"]}\n']
        logger.info(self.db.list_collection_names())
        logger.info("Successfully logged into Mongo")

    def get_user(self, id: str) -> MongoUser:
        return MongoUser(self.db.profiles.find_one({"UserId": id}), self.db)
