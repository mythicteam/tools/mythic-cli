import os

from google.cloud import storage

from mythictable import logger

class GCPConnector:
    def __init__(self) -> None:
        self.client = storage.Client()
        self.bucket = self.client.bucket(os.environ['MT_GCP_BUCKET'])

    def get_user_blobs(self, id: str):
        if id == '':
            raise RuntimeError

        return self.bucket.list_blobs(prefix=id)

    def delete_user_blobs(self, id: str, dry_run: bool):
        blobs = self.get_user_blobs(id)
        for blob in blobs:
            if dry_run:
                logger.warn(f"Skipping delete of {blob.name} due to dry run")
            else:
                blob.delete()

            
